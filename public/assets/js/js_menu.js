function bienvenida(usuario = null) {
	Swal.fire({
		toast: true,
		position: "top",
		icon: "success",
		title: "Bienvenid@ " + usuario,
		showConfirmButton: false,
		timer: 3500,
		timerProgressBar: true,
	});
}

$(document).ready(function (e) {
	bienvenida(Usuario);
});
