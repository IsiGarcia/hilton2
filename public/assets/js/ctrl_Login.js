$(document).on("click", "#btn-formulario", function (e) {
	e.preventDefault();

	// HACE VISIBLE EL SPINNER DE CARGA
	$("#carga").toggleClass("d-flex");
	$("#carga").toggleClass("d-none");

	$.ajax({
		// LA RUTA MANDA A LLAMAR A LA FUNCION getSesion DEL CONTROLADOR Sesion
		// LA VARIABLE baseUrl SE TRAE DESDE LA VISTA LOGIN (v_login)
		url: baseUrl + "sesion/getSesion",
		data: $("#LoginForm").serialize(),
		type: "POST",
		dataType: "JSON",

		success: function (data, txtStatus, jqXHR) {
			// HACE INVISIBLE EL SPINNER DE CARGA
			$("#carga").toggleClass("d-none");
			$("#carga").toggleClass("d-flex");

			if (data.head == "Success") {
				// REDIRECCIONA AL CONTROLADOR DEL MENU
				$(location).attr("href", baseUrl + "menu");
			} else {
				console.warn("Sucedio un error: " + data.body);

				Swal.fire({
					position: "center",
					icon: "error",
					title: "Error",
					text: data.body,
					showConfirmButton: true,
				});
			}
		},
		error: function (error) {
			// HACE INVISIBLE EL SPINNER DE CARGA
			$("#carga").toggleClass("d-none");
			$("#carga").toggleClass("d-flex");

			console.error("Sucedio un error: " + error);

			Swal.fire({
				position: "center",
				icon: "error",
				title: "Error",
				text: "Lo sentimos, ha ocurrido un error.",
				showConfirmButton: true,
			});
		},
	});
});
