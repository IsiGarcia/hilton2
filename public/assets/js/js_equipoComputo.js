$(document).on("click", "#btn-guardar", function (e) {
	e.preventDefault();

	// HACE VISIBLE EL SPINNER DE CARGA
	$("#carga").toggleClass("d-flex");
	$("#carga").toggleClass("d-none");

	$.ajax({
		url: baseUrl + "asignacion/registroComputo",
		data: $("#formEquipoComputo").serialize(),
		type: "POST",
		dataType: "JSON",

		success: function (data, txtStatus, jqXHR) {
			if (data.head == "Success") {
				$("#carga").toggleClass("d-flex");
				$("#carga").toggleClass("d-none");

				Swal.fire({
					position: "center",
					icon: "success",
					title: "¡Excelente!",
					text: data.body,
					showConfirmButton: false,
					timer: 2000,
				});

				// RESETEA EL FORMULARIO
				$("#formEquipoComputo").trigger("reset");
			} else {
				$("#carga").toggleClass("d-flex");
				$("#carga").toggleClass("d-none");

				Swal.fire({
					position: "center",
					icon: "error",
					title: "Error",
					showConfirmButton: true,
					html: data.body,
				});
			}
		},

		error: function (error) {
			$("#carga").toggleClass("d-flex");
			$("#carga").toggleClass("d-none");

			Swal.fire({
				position: "center",
				icon: "error",
				title: "Error",
				text:
					"Lo sentimos ha ocurrido un error, verifique su conexion a internet",
				showConfirmButton: true,
			});

			console.error(error);
		},
	});
});
