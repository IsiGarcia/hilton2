<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_asignacion extends CI_Model {

    // CONSTRUCTOR
    public function __construct() {
        parent::__construct();

        // CONEXION A LA BD
        $this->load->database();
        
    }

    public function INS_registroComputo($data) {
        // RE REALIZA LA ISERCION A LA BD (EL PRIMER PARAMETRO ES EL NOMBRE DE LA TABLA Y EL SEGUNDO LOS DATOS A INSERTAR)
        $this->db->insert("datos_usuario", $data);

        // RETORNA EL ID INSERTADO
        return $this->db->insert_id();
    }
}