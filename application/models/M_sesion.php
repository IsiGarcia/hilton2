<?php defined('BASEPATH') OR exit('No direct script access allowed');

class M_sesion extends CI_Model {

	// SE RECOMIENDA QUE TODA CLASE LLEVE SU PROPIO CONTRUCTOR, AQUE SE IMPORTAN LAS LIBRERIAS Y HELPERS QUE SE UTILIZAN EN MAS DE UNA FUNCION PARA NO IMPORTARLAS MAS DE UNA VES Y TAMBIEN TODO AQUELLO QUE SE NECESITE ANTES DE INICIAR CUALQUIER ACCION (REQUERIMIENTOS)
	public function __construct() {
        parent::__construct();
        
        // CARGA LA CONEXION A LA BD (REVISAR LA CONFIGURACION EN CONFIG ARCHIVO DATABASE)
        $this->load->database();
	}

	public function CNS_Sesion($datos) {

		// AQUI SE SELECCIONAN LAS COLUMNAS QUE RETORNARA LA BD PARA LA VISUALIZACION
		$this->db->select('Nombre');
		// LA CONDICION WHERE
		$this->db->where($datos);
		// NOMBRE DE LA TABLA A COMSULTAR
		$this->db->from('admin');

		// SE OBTIENE LOS RESULTADOS
		$query = $this->db->get();

		// RETORNA EL NUMERO DE REGISTROS
		return $query->row();

	}
}