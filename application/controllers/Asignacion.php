<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Asignacion extends CI_Controller {

    // CONSTRUCTOR
    public function __construct() {
        parent::__construct();

        // HELPERS
        $this->load->helper('url');

        // LIBRARIES
        $this->load->library('session');
        $this->load->library('form_validation');

        // MODELS
        $this->load->model('M_asignacion');
    }

    public function index() {

        // GUARDA LOS DATOS DE LA SESION
        $sesion = $this->session->userdata('Usuario');

        // VERIFICA QUE LOS DATOS DE LA SESION EXISTAN Y NO SEAN NULOS
        if(isset($sesion)) {

            // DATOS PARA ENVIAR A LAS VISTAS
            $resources = array(
                "style" => "formularios.css",
                "title" => "Equipo de computo",
                "script" => "js_equipoComputo.js"
            );

            // SE CARGAN LAS VISTAS PERTINENTES
            $this->load->view('templates/head', $resources);
            $this->load->view('templates/navBarAsignacion');
            $this->load->view('v_equipoComputo');
            $this->load->view('templates/footer');

        } else {
            // SI LA SESION NO EXISTE REDIRIGE A SESION
            redirect(base_url("sesion"));
        }

    }

    public function equipoMovil() {
        
        // GUARDA LOS DATOS DE SESION
        $sesion = $this->session->userdata('Usuario');

        if(isset($sesion)) {

            // DATOS PARA LAS VISTAS
            $resources = array(

            );

            // SE CARGAN LAS VISTAS
            $this->load->view('templates/head', $resources);
            $this->load->view('templates/navBarAsignacion');
            // $this->load->view('v_asignacion');
            $this->load->view('templates/footer');

        } else {
            redirect(base_url("sesion"));
        }
    }

    public function registroComputo() {
        // GUARDA/RESCATA LOS DATOS DE SESION
        $sesion = $this->session->userdata("Usuario");

        // VERIFICA QUE LA SESION EXISTA
        if (isset($sesion)) {
            // SE ESTABLACEN LAS REGLAS DEL FORMULARIO
                $this->form_validation->set_rules("Fecha", "Fecha", "required");
                // SECCION DATOS DEL USUARIO
                $this->form_validation->set_rules("Numcol", "No. Colaborador", "required|numeric");
                $this->form_validation->set_rules("Nombre", "Nombre", "required");
                $this->form_validation->set_rules("Puesto", "Puesto", "required");
                $this->form_validation->set_rules("Jefe", "Jefe Inmediato", "required");
                $this->form_validation->set_rules("ApPaterno", "Ap. Paterno", "required");
                $this->form_validation->set_rules("Area", "Área", "required");
                $this->form_validation->set_rules("Hotel", "Hotel", "required");
                $this->form_validation->set_rules("ApMaterno", "Ap. Materno", "required");
                $this->form_validation->set_rules("Solicitud", "Solicitud", "required");
                // SECCION CARACTERISTICAS EQUIPO COMPUTO
                $this->form_validation->set_rules("Equipo", "Tipo", "required");
                $this->form_validation->set_rules("Disco", "Disco Duro", "required");
                $this->form_validation->set_rules("Monitor", "Monitor/Pantalla", "required");
                $this->form_validation->set_rules("Marca", "Marca", "required");
                $this->form_validation->set_rules("Ram", "Mem. Ram", "required");
                $this->form_validation->set_rules("Tag", "Service TAG", "required");
                $this->form_validation->set_rules("Modelo", "Modelo", "required");
                $this->form_validation->set_rules("Licencia", "Licencia", "required");
                $this->form_validation->set_rules("Impresora", "Impresora", "required");
                $this->form_validation->set_rules("Serie", "No. Serie", "required");
                $this->form_validation->set_rules("Serialim", "Serial", "required");
                $this->form_validation->set_rules("Observacion", "Observaciones", "required");
                // SECCION CHECK LIST CONFIGURACION EQUIPO
                $this->form_validation->set_rules("Defaultt", "Default", "required");
                $this->form_validation->set_rules("Entrega", "Nombre", "required");
                $this->form_validation->set_rules("Puesto_e", "Puesto", "required");
                $this->form_validation->set_rules("Sucursal_e", "Sucursal", "required");
                $this->form_validation->set_rules("Devolucion", "Nombre", "required");
                $this->form_validation->set_rules("PuestoD", "Puesto", "required");
                $this->form_validation->set_rules("SucursalD", "Sucursal", "required");
                $this->form_validation->set_rules("Particular", "Particular", "required");
                $this->form_validation->set_rules("RecibeE", "Nombre", "required");
                $this->form_validation->set_rules("PuestoE", "Puesto", "required");
                $this->form_validation->set_rules("SucursalE", "Sucursal", "required");
                $this->form_validation->set_rules("Obtiene", "Nombre", "required");
                $this->form_validation->set_rules("PuestoO", "Puesto", "required");
                $this->form_validation->set_rules("SucursalO", "Sucursal", "required");
            // FIN REGLAS DE FORMULARIO

            // SI LAS VALIDACIONES NO PASAN
            if ($this->form_validation->run() == false) {
                echo json_encode(array(
                    "head" => "Error",
                    "body" => validation_errors(" ","<br />")
                ));
    
                exit();
            } else {
                //SI CUMPLE CON LAS VALIDACIONES

                // (ESTO NO SE HACE), CUANDO LLEGUES A ESTE PUNTO PREGUNTAME PARA DECIRTE COMO SE HACE, (ESTO ES UNA "MEXICANADA")
                $resp = $this->M_asignacion->INS_registroComputo($_POST);

                // SI LA INSERCION SE REALIZA CORRECTAMENTE
                if ($resp) {
                    echo json_encode(array(
                        "head" => "Success",
                        "body" => "Registro insertado correctamente"
                    ));
                } else {
                    echo json_encode(array(
                        "head" => "Error",
                        "body" => "Lo sentimos, ha ocurrido un error al registrar los datos en la Base de datos"
                    ));
                }
            }
        } else {
            // SI LA SESION NO EXISTE SE REDIRIGE AL LOGIN
            redirect(base_url("sesion"));
        }

    }
}