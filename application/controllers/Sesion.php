<?php defined('BASEPATH') OR exit('No direct script access allowed');

// ESTE ES EL CONTROLADOR POR DEFECTO A INICIAR (REVISAR EN LA CARPETA CONFIG ROUTES LINEA 52)
class Sesion extends CI_Controller {

	// SE RECOMIENDA QUE TODA CLASE LLEVE SU PROPIO CONTRUCTOR, AQUE SE IMPORTAN LAS LIBRERIAS Y HELPERS QUE SE UTILIZAN EN MAS DE UNA FUNCION PARA NO IMPORTARLAS MAS DE UNA VES Y TAMBIEN TODO AQUELLO QUE SE NECESITE ANTES DE INICIAR CUALQUIER ACCION (REQUERIMIENTOS)
	public function __construct() {
		parent::__construct();

		// HELPERS
		$this->load->helper('url');

		// LIBRARIES
		$this->load->library('form_validation');
		$this->load->library('session');

		// MODELOS
		$this->load->model("M_sesion");
	}

	// CUALQUIER FUNCION QUE SE LLAME INDEX SERÁ LA QUE SE INICIE POR DEFECTO SI NO SE ESPECIFICA UNA FUNCION A EJECUTAR
	public function index() {

		// CONSULTA Y GUARDA LOS DATOS DE SESION
		$sesion = $this->session->userdata('Usuario');

		// SI LA SESION EXISTE Y NO ES NULA
		if (isset($sesion)) {
			// REDIRIGE AL PANEL/MENU
			redirect(base_url("menu"));
		} else {

			// LIBRERIAS A UTILIZAR
			$libs = array(
				"particles/js/particles.min.js",
				"particles/js/app.js"
			);
			
			// ARREGLO PARA LAS VISTAS
			$resources = array(
				"title" => "Login",
				"style" => "LOGINS.css",
				"script" => "ctrl_Login.js",
				"libs" => $libs
			);

			// HEAD
			$this->load->view('templates/head', $resources);
			//VISTA LOGIN
			$this->load->view('v_login');
			// FOOTER
			$this->load->view('templates/footer', $resources);
		}
	}

	public function getSesion() {

		// ESTABLECE LAS REGLAS DEL FORMULARIO PARA VALIDACION
		$this->form_validation->set_rules("usuario", "Usuario", "trim|required|max_length[30]");
		$this->form_validation->set_rules("contrasena", "Contraseña", "trim|required|max_length[20]");

		// SI EL FORMULARIO NO PASA LAS VALIDACIONES
		if($this->form_validation->run() == false) {
			echo json_encode(array(
				"head" => "Error",
				"body" => validation_errors(" ","\n")
			));

			exit();
		} else {
			// SE PREPARA EL ARREGLO A ENVIAR PARA LA CONSULTA EN LA BD DESDE EL MODELO
			$data = array(
				"usuario" => $this->input->post("usuario"),
				"contrasena" => md5($this->input->post("contrasena"))
			);

			// SE MANDA EL ARREGLO AL MODELO PARA SU CONSULTA Y SE GUARDA LA RESPUESTA
			$resp = $this->M_sesion->CNS_Sesion($data);

			// SI RECIBE UNA RESPUESTA DEL MODELO
			if($resp) {

				// SE PREPARA EL ARREGLO PARA GUARDAR LOS DATOS DE SESION
				$session = array(
					"Usuario" => $resp->Nombre
				);

				// SE CREA LA SESION
				$this->session->set_userdata($session);

				echo json_encode(array(
					"head" => "Success",
					"body" => $session
				));

			} else {
				echo json_encode(array(
					"head" => "Error",
					"body" => "No se ha podido verificar el usuario en la base de datos, verifique que los datos ingresados sean correctos."
				));
			}
		}
	}

	public function logOut() {

		// BORRA EL USUARIO DE LA SESION
		$this->session->unset_userdata('Usuario');

		redirect(base_url("sesion"));
	}
}
