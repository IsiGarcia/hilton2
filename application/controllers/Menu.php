<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Menu extends CI_Controller {

    // CONSTRUCTOR
    public function __construct() {
        parent::__construct();

        // HELPERS
        $this->load->helper('url');

        // LIBRARIES
        $this->load->library('session');
    }

    public function index() {

        // GUARDA LOS DATOS DE LA SESION
        $sesion = $this->session->userdata('Usuario');

        // VERIFICA LA EXISTENCIA DE LA SESION
        if(isset($sesion)) {
            // SI LA SESION EXISTE CARGA LA VISTA DEL MENU O PANEL PRINCIPAL

            // DATOS PARA ENVIAR A LAS VISTAS
            $resources = array(
                "title" => "Menú",
                "style" => "menu.css",
                "script" => "js_menu.js",
                "user" => $sesion
            );

            // HEAD
            $this->load->view('templates/head', $resources);
            // NAVBAR DEL MENU
            $this->load->view('templates/navBarMenu');
            //VISTA MENU
            $this->load->view('v_menu');
            // FOOTER
            $this->load->view('templates/footer', $resources);
        } else {
            // SI LA SESION NO EXISTE REDIRIGE A SESION
            redirect(base_url("sesion"));
        }

    }
}