    <!-- SECCION TITULO -->
    <section>
        <div class="container-fluid bg-white">
            <div class="row my-3">
                <div class="col-12">
                    <p class="encabezado">Hilton Hotels & Resort PV</p>
                </div>
            </div>
        </div>
    </section>
    <!-- FIN SECCION TITULO -->

    <!-- SECCION BOTONES -->
    <section>
        <div class="container-fluid pt-5">
            <div class="row justify-content-center">

                <!-- BOTON INVENTARIO -->
                <a class="col-sm-5 col-md-3 mt-4 mt-md-0" href="#">
                    <div class="contenedor tarjeta col-12 p-3" id="uno">
                        <div class="row align-content-center">
                            <img class="icon img-fluid col-4 col-sm-6 mx-auto p-2 p-sm-4" src="<?= base_url()?>public/assets/img/inventario.png">
                        </div>
                    
                        <div class="row align-content-center">
                            <p class="texto col-12 text-center text-white">Inventario</p>
                        </div>
                    </div>
                </a>
                <!-- FIN BOTON INVENTARIO -->

                <!-- BOTON ASIGNACION -->
                <a class="col-sm-5 col-md-3 mt-4 mt-md-0" href="<?= base_url()?>asignacion">
                    <div class="contenedor col-12 tarjeta p-3" id="dos">
                        <div class="row align-content-center">
                            <img class="icon img-fluid col-4 col-sm-6 mx-auto p-2 p-sm-4" src="<?= base_url()?>public/assets/img/computadora.png">
                        </div>

                        <div class="row align-content-center">
                            <p class="texto col-12 text-center text-white">Asignación</p>
                        </div>
                    </div>
                </a>
                <!-- FIN BOTON ASIGNACION -->

                <!-- BOTON CREDENCIALES -->
                <a class="col-sm-5 col-md-3 mt-4 mt-sm-4 mt-md-0" href="<?= base_url()?>credenciales">
                    <div class="contenedor tarjeta col-12 p-3" id="tres">
                        <div class="row align-content-center">
                            <img class="icon img-fluid col-4 col-sm-6 mx-auto p-2 p-sm-4" src="<?= base_url()?>public/assets/img/seguridad.png">
                        </div>

                        <div class="row align-content-center">
                            <p class="texto col-12 text-center text-white" >Credenciales</p>
                        </div>
                    </div>
                </a>
                <!-- FIN BOTON CREDENCIALES -->

            </div>
        </div>
    </section>
    <!-- FINSECCION BOTONES -->