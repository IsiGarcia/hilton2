    <!-- SECCION NAVBAR -->
    <section>
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar navbar-dark bg-dark col-12">
                    <!-- BOTON IMAGEN -->
                    <a class="navbar-brand col-7 col-sm-2 m-0 p-0 d-flex align-items-center justify-content-center" href="<?= base_url()?>menu">
                        <img src="<?= base_url()?>public/assets/img/hilton.png" class="img-fluid col-12 col-xl-11" alt="">
                    </a>

                    <!-- BOTON SALIR -->
                    <div class="col-5 col-sm-10 text-right">
                        <a class="btn btn-outline-light"  href="<?= base_url()?>sesion/logOut">Salir</a>
                    </div>
                    <!-- FIN BOTON SALIR -->

                </nav>
            </div>
        </div>
    </section>
    <!-- FIN SECCION NAVBAR -->
    
    