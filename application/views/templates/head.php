<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <?php if(isset($title)) { ?>
        <title><?= $title ?></title>
    <?php } else { ?>
        <title></title>
    <?php } ?>

    <!-- STYLE SHEETS -->

    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="<?= base_url()?>public/libs/bootstrap/dist/css/bootstrap.css" />
    <!-- FONTAWESOME -->
    <link rel="stylesheet" href="<?= base_url()?>public/libs/fontawesome/css/all.min.css" />
    <!-- DATATABLE -->
    <link rel="stylesheet" href="<?= base_url()?>public/libs/datatables/datatables.min.css" />
    <link rel="stylesheet" href="<?= base_url()?>public/libs/datatables/DataTables-1.10.18/css/dataTables.bootstrap4.min.css" />

    <?php if(isset($style)) { ?>
        <link rel="stylesheet" href="<?= base_url()?>public/assets/css/<?= $style?>" />
    <?php } ?>
</head>
<body>
