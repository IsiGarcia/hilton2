    <!-- SECCION NAVBAR -->
    <section>
        <div class="container-fluid">
            <div class="row">
                <nav class="navbar navbar-dark bg-dark col-12">
                    <!-- BOTON IMAGEN -->
                    <a class="navbar-brand col-5 col-sm-2 m-0 p-0 d-flex align-items-center justify-content-center" href="<?= base_url()?>menu">
                        <img src="<?= base_url()?>public/assets/img/hilton.png" class="img-fluid col-12 col-xl-11" alt="">
                    </a>

                    <!-- SECCION OPCIONES => SM EN BOOTSTRAP -->
                    <div class="col-10 d-none d-sm-block">
                        <div class="row justify-content-center">
                            <!-- OPCIONES -->
                            <div class="col-10 text-center d-flex align-items-center justify-content-center">
                                <a class="nav-item text-white px-1 mx-auto" href="<?= base_url()?>asignacion">Equipo de Cómputo</a>
                                <a class="nav-item text-white px-1 mx-auto" href="<?= base_url()?>asignacion/equipoMovil">Equipo Móvil</a>
                                <a class="nav-item text-white px-1 mx-auto" href="http://localhost/HILTON/Asignacion/formulario.php">Consulta Equipo</a>
                                <a class="nav-item text-white px-1 mx-auto" href="http://localhost/HILTON/Asignacion/movformulario.php">Consulta Movil</a>
                            </div>
                            <!-- FIN OPCIONES -->

                            <!-- BOTON SALIR -->
                            <div class="col-2 text-center d-flex align-items-center justify-content-center">
                                <a class="btn btn-outline-light"  href="<?= base_url()?>sesion/logOut">Salir</a>
                            </div>
                        </div>
                    </div>
                    <!-- FIN SECCION OPCIONES => SM EN BOOTSTRAP -->

                    <!-- BOTON MENU -->
                    <div class="col-7 d-block d-sm-none text-white text-right" data-toggle="collapse" data-target="#opciones">
                        <i class="fas fa-bars fa-2x"></i>
                    </div>
                    <!-- FIN BOTON MENU -->

                    <!-- SECCION OPCIONES < SM EN BOOTSTRAP -->
                    <div id="opciones" class="col-12 collapse">
                        <div class="row justify-content-center">
                            <!-- OPCIONES -->
                            <div class="col-12 text-center">
                                <a class="nav-item text-white px-1 mx-auto col-11" href="<?= base_url()?>asignacion">Equipo de Cómputo</a> <br />
                                <a class="nav-item text-white px-1 mx-auto col-11" href="<?= base_url()?>asignacion/equipoMovil">Equipo Móvil</a> <br />
                                <a class="nav-item text-white px-1 mx-auto col-11" href="http://localhost/HILTON/Asignacion/formulario.php">Consulta Equipo</a> <br />
                                <a class="nav-item text-white px-1 mx-auto col-11" href="http://localhost/HILTON/Asignacion/movformulario.php">Consulta Movil</a> <br />
                            </div>
                            <!-- FIN OPCIONES -->

                            <!-- BOTON SALIR -->
                            <div class="col-12 text-center d-flex align-items-center justify-content-center">
                                <a class="btn btn-outline-light col-3"  href="<?= base_url()?>sesion/logOut">Salir</a>
                            </div>
                        </div>
                    </div>
                    <!-- FIN SECCION OPCIONES < SM EN BOOTSTRAP -->

                </nav>
            </div>
        </div>
    </section>
    <!-- FIN SECCION NAVBAR -->
