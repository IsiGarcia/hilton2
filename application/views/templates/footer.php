	<!-- ARCHIVOS JS -->
	
	<!-- GUARDA LA BASE URL -->
    <script>
		var baseUrl = "<?= base_url()?>";
	</script>

	<!-- GUARDA EL NOMBRE DEL USUARIO SI EXISTE -->
	<?php if(isset($user)) { ?>
        <script>
			var Usuario = "<?= $user?>";
		</script>
    <?php } ?>

	<script src="<?= base_url()?>public/libs/jquery-3.4.1.min.js"></script>
	<script src="<?= base_url()?>public/libs/popper-1.16.0.min.js"></script>
	<script src="<?= base_url()?>public/libs/bootstrap/dist/js/bootstrap.js"></script>
	
	<script src="<?= base_url()?>public/libs/sweetAlert2.js"></script>

	<!-- LIBRERIAS -->
	<?php if(isset($libs)) { 
		foreach($libs as $lib => $value) {?>
        	<script src="<?= base_url()?>public/libs/<?= $value?>"></script>
		<?php }
	} ?>
    
	<!-- SCRIPTS -->
    <?php if(isset($script)) { ?>
        <script src="<?= base_url()?>public/assets/js/<?= $script?>"></script>
    <?php } ?>
	
</body>
</html>