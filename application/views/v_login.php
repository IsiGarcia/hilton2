	<!-- SECCION PARTICULAS -->
	<section>
		<div class="container-fluid">
			<div class="row">
				<div class="col-12 m-0 p-0 w-100 h-100">
					<div id="particles-js"></div>
				</div>
			</div>
		</div>
	</section>
	<!-- FIN SECCION PARTICULAS -->

	<!-- SECCION SPIN LOGIN -->
	<div id="carga" class="container-fluid d-none align-items-center justify-content-center vh-100">
		<i class="fas fa-sync fa-spin fa-5x text-white"></i>
	</div>
	<!-- FIN SECCION SPIN LOGIN -->

	<!-- SECCIOPN FORMULARIO -->
	<section id="formulario">
		<div class="container-fluid">
			<div class="row justify-content-center vh-100">
				<!-- INICIO DEL FORMULARIO -->
				<div class="login-box col-11 col-sm-10 col-md-6 col-lg-4 m-auto">
					<!-- ENCABEZADO DEL FORMULARIO -->
					<div class="col-12">
						<!-- IMAGEN -->
						<div class="col-12 text-center my-3">
							<img class="img-fluid col-10 col-sm-7 col-md-9" src="<?= base_url()?>public/assets/img/hilton.png" />
						</div>
						<!-- FIN IMAGEN -->

						<!-- TITULO -->
						<div class="col-12">
							<h1>
								<small>Puerto Vallarta</small>
							</h1>
						</div>
						<!-- FIN TITULO -->
					</div>
					<!-- FIN ENCABEZADO FORMULARIO -->

					<!-- CUERPO DEL FORMULARIO -->
					<div class="col-12">
						<form id="LoginForm">

							<!-- Usuario INPUT -->
							<div class="row justify-content-center mb-3">
								<input
								class="col-12 text-center text-white"
								id="usuario"
								type="text"
								placeholder="Usuario"
								name="usuario"
								autocomplete="off"
								required
								/>
							</div>
							
							<!-- Contraseña INPUT -->
							<div class="row justify-content-center mb-3">
								<input
								class="col-12 text-center text-white"
								id="contrasena"
								type="password"
								placeholder="Contraseña"
								name="contrasena"
								autocomplete="off"
								required
								/>
							</div>
							
							<!-- BOTON DEL FORMULARIO -->
							<div class="row justify-content-center mb-3">
								<input class="col-12 text-center text-white" id="btn-formulario" type="submit" value="Iniciar Sesion" />
							</div>
						</form>
					</div>
					<!-- FIN CUERPO FORMULARIO -->
				</div>
				<!-- FIN DEL FORMULARIO -->
			</div>
		</div>
	</section>
	<!-- FIN SECCION FORMULARIO -->
