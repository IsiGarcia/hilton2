    <!-- FORMULARIO EQUIPOCOMPUTO -->
    <section>
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <form id="formEquipoComputo" class="bg-light my-5">

                        <!-- FECHA -->
                        <div class="col-12 d-flex justify-content-end p-3">
                            <input  class="form-control col-6 col-md-3" type="date" name="Fecha" placeholder="Fecha" id="Fecha">
                        </div>
                        <!-- FIN FECHA -->

                        <!-- SECCION DATOS DEL USUARIO -->
                        <div class="col-12">
                            <!-- ENCABEZADO -->
                            <div class="bg-info text-center rounded">
                                <p class="encabezado text-white py-2">
                                    <strong>Datos del Usuario</strong>
                                </p>
                            </div>
                            <!-- FIN ENCABEZADO -->

                            <!-- CUERPO -->
                            <div class="row justify-content-center">
                                <!-- COLUMNA 1 -->
                                <div class="col-10 col-md-4 p-0">
                                    <!-- N° COLABORADOR -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">No. Colaborador</label>
                                        <input  class="form-control col-12" type="text" name="Numcol" placeholder="No. Colaborador" id="Numcol">
                                    </div>

                                    <!-- NOMBRE'S -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Nombre(s)</label>
                                        <input  class="form-control col-12" type="text" name="Nombre" placeholder="Nombre(s)" id="Nombre">
                                    </div>

                                    <!-- PUESTO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Puesto</label>
                                        <input  class="form-control" type="text" name="Puesto" placeholder="Puesto" id="Puesto">
                                    </div>

                                    <!-- JEFE INMEDIATO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Jefe Inmediato</label>
                                        <input  class="form-control" type="text" name="Jefe" placeholder="Jefe Inmediato" id="Jefes">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 1 -->

                                <!-- COLUMNA 2 -->
                                <div class="col-10 col-md-4 p-0">
                                    <!-- CELDA VACIA -->
                                    <div class="celdaVacia d-none d-md-block"></div>

                                    <!-- APELLIDO PATERNO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Ap. Paterno</label>
                                        <input  class="form-control" type="text" name="ApPaterno" placeholder="Ap. Paterno" id="ApPaterno">
                                    </div>

                                    <!-- AREA -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Área</label>
                                        <input  class="form-control" type="text" name="Area" placeholder="Área"id="Area">
                                    </div>

                                    <!-- HOTEL -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Hotel</label>
                                        <input  class="form-control" type="text" name="Hotel" placeholder="Hotel" id="Hotel">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 2 -->

                                <!-- COLUMNA 3 -->
                                <div class="col-10 col-md-4 p-0">
                                    <!-- CELDA VACIA -->
                                    <div class="celdaVacia d-none d-md-block"></div>

                                    <!-- APELLIDO MATERNO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Ap. Materno</label>
                                        <input  class="form-control" type="text" name="ApMaterno" placeholder="Ap. Materno" id="ApMaterno">
                                    </div>

                                    <!-- SOLICITUD -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Solicitud</label>
                                        <input  class="form-control"type="text" name="Solicitud" placeholder="Solicitud" id="Solicitud">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 3 -->
                            </div>
                            <!-- FIN CUERPO -->
                        </div>
                        <!-- FIN SECCION DATOS DEL USUARIO -->

                        <!-- SECCION CARACTERISTICAS EQUIPO COMPUTO -->
                        <div class="col-12">
                            <!-- ENCABEZADO -->
                            <div class="bg-info text-center rounded">
                                <p class="encabezado text-white py-2">
                                    <strong>Características de Equipos de Cómputo</strong>
                                </p>
                            </div>
                            <!-- FIN ENCABEZADO -->

                            <!-- CUERPO -->
                            <div class="row justify-content-center">
                                <!-- COLUMNA 1 -->
                                <div class="col-10 col-sm-6 col-md-3 p-0">
                                    <!-- TIPO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Tipo</label>
                                        <input  class="form-control" type="text" name="Equipo" placeholder="Tipo" id="Equipo">
                                    </div>

                                    <!-- DISCO DURO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Disco Duro</label>
                                        <input  class="form-control" type="text" name="Disco" placeholder="Disco Duro" id="Disco">
                                    </div>

                                    <!-- MONITOR/PANTALLA -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Monitor/Pantalla</label>
                                        <input  class="form-control" type="text" name="Monitor" placeholder="Monitor/Pantalla" id="Monitor">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 1 -->

                                <!-- COLUMNA 2 -->
                                <div class="col-10 col-sm-6 col-md-3 p-0">
                                    <!-- MARCA -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Marca</label>
                                        <input  class="form-control" type="text" name="Marca" placeholder="Marca" id="Marca">
                                    </div>

                                    <!-- MEMORIA RAM -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Mem. Ram</label>
                                        <input  class="form-control" type="text" name="Ram" placeholder="Mem. Ram"id="Ram">
                                    </div>

                                    <!-- SERVICE TAG -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Service TAG</label>
                                        <input  class="form-control" type="text" name="Tag" placeholder="Service TAG" id="Tag">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 2 -->

                                <!-- COLUMNA 3 -->
                                <div class="col-10 col-sm-6 col-md-3 p-0">
                                    <!-- MODELO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Modelo</label>
                                        <input  class="form-control" type="text" name="Modelo" placeholder="Modelo" id="Modelo">
                                    </div>

                                    <!-- LICENCIA -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Licencia</label>
                                        <input  class="form-control" type="text" name="Licencia" placeholder="Licencia" id="Licencia">
                                    </div>

                                    <!-- IMPRESORA -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Impresora</label>
                                        <input  class="form-control" type="text" name="Impresora" placeholder="Impresora" id="Impresora">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 3 -->

                                <!-- COLUMNA 4 -->
                                <div class="col-10 col-sm-6 col-md-3 p-0">
                                    <!-- N° SERIE -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">No. Serie</label>
                                        <input  class="form-control" type="text" name="Serie" placeholder="No. Serie" id="Serie">
                                    </div>

                                    <!-- CELDA VACIA -->
                                    <div class="celdaVacia d-none d-sm-block"></div>

                                    <!-- SERIAL -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Serial</label>
                                        <input  class="form-control" type="text" name="Serialim" placeholder="Serial" id="Serialim">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 4 -->
                            </div>
                            <!-- FIN CUERPO -->

                            <!-- FILA INFERIOR -->
                            <div class="row justify-content-center">
                                <!-- OBSERVACIONES -->
                                <div class="col-10 col-sm-12 pr-3 pb-3">
                                    <label class="col-10 col-sm-11 my-auto">Observaciones</label>
                                    <textarea  class="form-control" rows="3" type="text" name="Observacion" placeholder="Observaciones de Entrega/Devolución" id="Observacion"></textarea>
                                </div>
                            </div>
                            <!-- FIN FILA INFERIOR -->
                        </div>
                        <!-- FIN SECCION CARACTERISTICAS EQUIPO COMPUTO -->

                        <!-- SECCION CHECK LIST CONFIGURACION EQUIPO -->
                        <div class="col-12">
                            <!-- ENCABEZADO -->
                            <div class="bg-info text-center rounded">
                                <p class="encabezado text-white py-2">
                                    <strong>Checklist de configuración del Equipo</strong>
                                </p>
                            </div>
                            <!-- FIN ENCABEZADO -->

                            <!-- CUERPO -->
                            <div class="col-12 d-flex">
                                <!-- COLUMNA 1 -->
                                <div class="col-6 p-2">
                                    <!-- DEFAULT -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Default</label>
                                        <input  class="form-control" type="text" name="Defaultt" placeholder="" id="Default">
                                    </div>

                                    <!-- SUBENCABEZADO ENTREGA -->
                                    <div class="bg-info text-center rounded col-12 pr-3">
                                        <p class="subEncabezado text-white py-2">
                                            <strong>Entrega</strong>
                                        </p>
                                    </div>

                                    <!-- NOMBRE -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Nombre</label>
                                        <input  class="form-control" type="text" name="Entrega" placeholder="Nombre" id="Entrega">
                                    </div>

                                    <!-- PUESTO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Puesto</label>
                                        <input  class="form-control" type="text" name="Puesto_e" placeholder="Puesto" id="Puesto_e">
                                    </div>

                                    <!-- SUCURSAL -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Sucursal</label>
                                        <input  class="form-control" type="text" name="Sucursal_e" placeholder="Sucursal" id="Sucursal_e">
                                    </div>

                                    <!-- SUBENCABEZADO DEVUELVE -->
                                    <div class="bg-info text-center rounded col-12 pr-3">
                                        <p class="subEncabezado text-white py-2">
                                            <strong>Devuelve</strong>
                                        </p>
                                    </div>

                                    <!-- NOMBRE -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Nombre</label>
                                        <input  class="form-control" type="text" name="Devolucion" placeholder="Nombre" id="Devolucion">
                                    </div>

                                    <!-- PUESTO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Puesto</label>
                                        <input  class="form-control" type="text" name="PuestoD" placeholder="Puesto" id="PuestoD">
                                    </div>

                                    <!-- SUCURSAL -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Sucursal</label>
                                        <input  class="form-control" type="text" name="SucursalD" placeholder="Sucursal" id="SucursalD">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 1 -->

                                <!-- COLUMNA 2 -->
                                <div class="col-6 p-2">
                                    <!-- PARTICULAR -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Particular</label>
                                        <input  class="form-control" type="text" name="Particular" placeholder="" id="Particular">
                                    </div>

                                    <!-- SUBENCABEZADO RECIBE 1 -->
                                    <div class="bg-info text-center rounded col-12 pr-3">
                                        <p class="subEncabezado text-white py-2">
                                            <strong>Recibe</strong>
                                        </p>
                                    </div>

                                    <!-- NOMBRE -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Nombre</label>
                                        <input  class="form-control" type="text" name="RecibeE" placeholder="Nombre" id="RecibeE">
                                    </div>

                                    <!-- PUESTO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Puesto</label>
                                        <input  class="form-control" type="text" name="PuestoE" placeholder="Puesto" id="PuestoE">
                                    </div>

                                    <!-- SUCURSAL -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Sucursal</label>
                                        <input  class="form-control" type="text" name="SucursalE" placeholder="Sucursal" id="SucursalE">
                                    </div>

                                    <!-- SUBENCABEZADO RECIBE 2 -->
                                    <div class="bg-info text-center rounded col-12 pr-3">
                                        <p class="subEncabezado text-white py-2">
                                            <strong>Recibe</strong>
                                        </p>
                                    </div>

                                    <!-- NOMBRE -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Nombre</label>
                                        <input  class="form-control" type="text" name="Obtiene" placeholder="Nombre" id="Obtiene">
                                    </div>

                                    <!-- PUESTO -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Puesto</label>
                                        <input  class="form-control" type="text" name="PuestoO" placeholder="Puesto" id="PuestoO">
                                    </div>

                                    <!-- SUCURSAL -->
                                    <div class="col-12 pr-3 pb-3">
                                        <label class="col-11 my-auto">Sucursal</label>
                                        <input  class="form-control" type="text" name="SucursalO" placeholder="Sucursal" id="SucursaO">
                                    </div>
                                </div>
                                <!-- FIN COLUMNA 2 -->
                            </div>
                            <!-- FIN CUERPO -->
                        </div>
                        <!-- FIN SECCION CHECK LIST CONFIGURACION EQUIPO -->

                        <!-- BOTON GUARDAR -->
                        <div class="col-12 d-flex justify-content-end">
                            <button class="btn btn-dark col-12 col-sm-4 col-md-2 my-4" id="btn-guardar">Guardar</button>
                        </div>
                        <!-- FIN BOTON GUARDAR -->
                    </form>
                </div>
            </div>
        </div>
    </section>
    <!-- FIN FORMULARIO EQUIPOCOMPUTO -->

    <!-- SECCION SPIN LOGIN -->
	<div id="carga" class="container-fluid d-none align-items-center justify-content-center vh-100">
		<i class="fas fa-sync fa-spin fa-5x text-white"></i>
	</div>
	<!-- FIN SECCION SPIN LOGIN -->
